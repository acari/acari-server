import socketIo      from 'socket.io'
import defaultConfig from './config'
import server        from './server'
import express       from 'express'
// import path          from 'path'

export default function({
  config,
  clientConnectedFn,
  clientDisconnectedFn,
  clientEventFn,
  clientActionFn,
  updateTickFn
}) {
  console.log('init acari server')

  const activeConfig = Object.assign(defaultConfig, config)

  const app = express()
  const httpServer = require('http').createServer(app)
  const port = process.env.PORT || activeConfig.server.port
  const io = socketIo(httpServer)
  httpServer.listen(port)

  io.on('connection', (socket) => {
    console.log('connection from client')

    server.clientConnected({
      io,
      socket,
      clientConnectedFn,
      clientDisconnectedFn,
      clientEventFn,
      clientActionFn
    })
  })

  server.updateTick({
    updateTickFn
  })

  app.use(express.static('public'))
  app.use(express.static('dist'))

  console.log('ACARI server running on port ' + port)

  // FIXME 11.10.2016 nviik - We should expose our API before initialization
  return {
    emitMessage: (type, msg) => {
      io.emit(type, msg)
    },
    destroyServer: () => {
      io.close()
    }
  }
}

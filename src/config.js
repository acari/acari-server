export default {
  server: {
    port: 3000
  },
  dbapi: {
    url: '127.0.0.1',
    port: 3001
  },
  tickInterval: 1000
}

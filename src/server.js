import defaultConfig from './config'
const clients = {}

function clientConnected({
  io,
  socket,
  clientConnectedFn,
  clientDisconnectedFn,
  clientEventFn,
  clientActionFn
}) {
  // console.log('socket', socket)
  console.log('new connection from', socket.id)
  clients[socket.id] = socket

  clientConnectedFn(socket.id)

  socket.on('event', (event) => {
    console.log('event from client', socket.id, event)
    clientEventFn(socket.id, event)
  })

  socket.on('action', (event) => {
    console.log('action from client', socket.id, event)
    clientActionFn(socket.id, event)
  })

  socket.on('disconnect', (event) => {
    console.log('client disconnected', socket.id, event)
    clientDisconnectedFn(socket.id)
    delete clients[socket.id]
  })
}

function updateTick({
  updateTickFn
}) {
  setInterval(() => {
    updateTickFn()
  }, defaultConfig.tickInterval)
}

export default {
  clientConnected,
  updateTick
}

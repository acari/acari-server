'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (_ref) {
  var config = _ref.config;
  var clientConnectedFn = _ref.clientConnectedFn;
  var clientDisconnectedFn = _ref.clientDisconnectedFn;
  var clientEventFn = _ref.clientEventFn;
  var clientActionFn = _ref.clientActionFn;
  var updateTickFn = _ref.updateTickFn;

  console.log('init acari server');

  var activeConfig = Object.assign(_config2.default, config);

  var app = (0, _express2.default)();
  var httpServer = require('http').createServer(app);
  var port = process.env.PORT || activeConfig.server.port;
  var io = (0, _socket2.default)(httpServer);
  httpServer.listen(port);

  io.on('connection', function (socket) {
    console.log('connection from client');

    _server2.default.clientConnected({
      io: io,
      socket: socket,
      clientConnectedFn: clientConnectedFn,
      clientDisconnectedFn: clientDisconnectedFn,
      clientEventFn: clientEventFn,
      clientActionFn: clientActionFn
    });
  });

  _server2.default.updateTick({
    updateTickFn: updateTickFn
  });

  app.use(_express2.default.static('public'));
  app.use(_express2.default.static('dist'));

  console.log('ACARI server running on port ' + port);

  // FIXME 11.10.2016 nviik - We should expose our API before initialization
  return {
    emitMessage: function emitMessage(type, msg) {
      io.emit(type, msg);
    },
    destroyServer: function destroyServer() {
      io.close();
    }
  };
};

var _socket = require('socket.io');

var _socket2 = _interopRequireDefault(_socket);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _server = require('./server');

var _server2 = _interopRequireDefault(_server);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
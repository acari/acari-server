'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var clients = {};

function clientConnected(_ref) {
  var io = _ref.io;
  var socket = _ref.socket;
  var clientConnectedFn = _ref.clientConnectedFn;
  var clientDisconnectedFn = _ref.clientDisconnectedFn;
  var clientEventFn = _ref.clientEventFn;
  var clientActionFn = _ref.clientActionFn;

  // console.log('socket', socket)
  console.log('new connection from', socket.id);
  clients[socket.id] = socket;

  clientConnectedFn(socket.id);

  socket.on('event', function (event) {
    console.log('event from client', socket.id, event);
    clientEventFn(socket.id, event);
  });

  socket.on('action', function (event) {
    console.log('action from client', socket.id, event);
    clientActionFn(socket.id, event);
  });

  socket.on('disconnect', function (event) {
    console.log('client disconnected', socket.id, event);
    clientDisconnectedFn(socket.id);
    delete clients[socket.id];
  });
}

function updateTick(_ref2) {
  var updateTickFn = _ref2.updateTickFn;

  setInterval(function () {
    updateTickFn();
  }, _config2.default.tickInterval);
}

exports.default = {
  clientConnected: clientConnected,
  updateTick: updateTick
};
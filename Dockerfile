FROM node:6.7.0

EXPOSE 3000

ADD . /acari-server
WORKDIR /acari-server
RUN npm install 
CMD node ./src/app.js
